#pragma once

#include <string_view>
#include <SOIL2.h>
#include <cassert>
#include <iostream>
#include <vector>
#include <filesystem>


class MapRead {
public:
    MapRead() = default;
    std::vector<std::vector<float>> Heights;
    
    virtual ~MapRead() = default;

    MapRead(std::string_view path) {
        LoadFromFile(path);
    }

    void LoadFromFile(std::string_view path) {

        int width, height, channels;
        unsigned char* data = SOIL_load_image(path.data(), &width, &height, &channels, SOIL_LOAD_L);
  
        LoadFromData(data, width, height);
        SOIL_free_image_data(data);
    }


    void LoadFromData(const unsigned char* data, int width, int height) {
        Heights.resize(height);
        for (int i = 0; i < height; ++i, data += width) {
            Heights[i].resize(width);
            for (int j = 0; j < width; ++j) {
                Heights[i][j] = static_cast<float>(data[j]) / 255;
            }
        }
    }

    float GetHeight(size_t i, size_t j) const {
        return Heights[i][j];
    }


    size_t GetYSize() const {
        return Heights.empty() ? 0 : Heights[0].size();
    }


    size_t GetXSize() const {
        return Heights.size();
    }



};