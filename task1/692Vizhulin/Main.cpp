#include <Application.hpp>
#include <Mesh.hpp>
#include <ShaderProgram.hpp>
#include <iostream>

class SampleApplication : public Application{
public:
    MeshPtr terrain;

    ShaderProgramPtr shader;

    void makeScene() override {
        Application::makeScene();
        _cameraMover = std::make_shared<FreeCameraMover>();
        
        terrain = makeTerrain();
        terrain->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(-50.0f, -80.0f, 0.0f)));

        shader = std::make_shared<ShaderProgram>("692VizhulinData1/color.vert", "692VizhulinData1/color.frag");

    }


    void draw() override{
        int width, height;
        
        glfwGetFramebufferSize(_window, &width, &height);
        glViewport(0, 0, width, height);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        
        shader->use();
        shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);
        shader->setMat4Uniform("modelMatrix", terrain->modelMatrix());

        terrain->draw();

    }
};

int main(){
    SampleApplication app;
    app.start();
    return 0;
}